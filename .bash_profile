#!/bin/bash

if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then exec startx; fi


# Added by Toolbox App
export PATH="$PATH:/home/munics/.local/share/JetBrains/Toolbox/scripts"