# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

for file in ~/.{path,bash_prompt,exports,bash_aliases,functions,extra}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;

HISTCONTROL=ignoreboth # don't put duplicate lines or lines starting with space in the history.
HISTSIZE=1000 # for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTFILESIZE=2000

### SHOPT
shopt -s histappend # append to the history file, don't overwrite it
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s checkwinsize # check the window size after each command and, if necessary, update the values of LINES and COLUMNS.

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

### CHANGE TITLE OF TERMINALS
case ${TERM} in
  xterm*|rxvt*|terminator|Eterm*|kitty|starship|aterm|kterm|gnome*|alacritty|st|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*} in ${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*} in ${PWD/#$HOME/\~}\033\\"'
    ;;
esac

# ignore upper and lowercase when TAB completion
# bind "set completion-ignore-case on"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# "bat" as manpager
# if [ -f /usr/bin/bat ]; then
#   export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# elif [ -f /usr/bin/batcat ]; then
#   export MANPAGER="sh -c 'col -bx | batcat -l man -p'"
# fi


### RANDOM COLOR SCRIPT ###
#if [ -d "/opt/shell-color-scripts/colorscripts" ]; then
#    colorscript random
#fi

### STARSHIP SHELL ###
if [ -f /usr/local/bin/starship ]; then
    eval "$(starship init bash)"
fi

# if [[ $(ps --no-header --pid=$PPID --format=cmd) != "fish" ]]
# then
# 	exec fish
# fi
